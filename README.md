The project was built with NextJs and Deploy on Vercel.

## Getting Started


To run the application locally:

```bash
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## App Url
- [https://luxor-tech.vercel.app](https://luxor-tech.vercel.app) - Moses Idowu Luxor Tech Challenge.


## Admin Login
```
username: admin
password: admin
```
import React from 'react';


const Loader = () => {
  return (
    <h1 style={{ color: '#fff', fontWeight: 'normal' }}>Loading...</h1>
  )
}

export default Loader;
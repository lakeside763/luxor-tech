import React from 'react';
import { FiLogOut } from 'react-icons/fi';
import styles from '../../styles/nav.module.scss';
import cookies from 'js-cookie';
import { useRouter } from 'next/router';

const Nav = () => {
  const router = useRouter();
  
  const logout = () => {
    cookies.remove('token');
    router.push('/');
  }

  return (
    <nav className={styles.app_nav}>
      <button onClick={logout}>
        <span>Logout</span>
        <span><FiLogOut /></span>
      </button>
    </nav>
  )
} 

export default Nav;
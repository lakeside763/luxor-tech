import React, { useState } from "react";
import styles from '../styles/login.module.scss';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useRouter } from "next/router";
import cookies from 'js-cookie';

type FormValues = {
  username: string,
  password: string,
}

const Login = () => {
  const [invalidError, setInvalidError] = useState(false);
  const router = useRouter();

  const schema = Yup.object({
    username: Yup.string().required('Username is required'),
    password: Yup.string().required('Password is required'),
  })

  const onSubmit = ({ username, password }: FormValues) => {
    if (username === 'admin' && password === 'admin') {
      setInvalidError(false);
      cookies.set('token', 'admin_login_123456');
      router.push('/pokemons');
    } else {
      setInvalidError(true);
    }
  }

  const { register, handleSubmit, formState: { errors } } = useForm<FormValues>({
    mode: 'onTouched',
    resolver: yupResolver(schema),
  });

  return(
    <div className={styles.login_wrapper}>
      <div className={styles.container}>
        <h1>Sign In</h1>
        <div className={styles.login_form}>
          {invalidError && (<span className={styles.invalid_credentials}>Invalid Username/Password</span>)}
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles.form_group}>
              <input 
                type='text' 
                placeholder='Enter Username' 
                {...register('username')} 
                required 
                name="username"
                className={errors.username?.message ? `${styles.errors_input}`: ''}
              />
              <span className={styles.errors}>{errors.username?.message}</span>
            </div>
            <div className={styles.form_group}>
              <input 
                type='password' 
                placeholder='Enter Password' 
                {...register('password')} 
                name='password'
                required
                className={errors.password?.message ? `${styles.errors_input}`: ''}
              />
              <span className={styles.errors}>{errors.password?.message}</span>
            </div>
            <div className={styles.form_group}>
              <button type='submit'>Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Login;
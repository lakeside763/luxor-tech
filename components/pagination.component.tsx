import React, { useState, useContext, useEffect } from 'react';
import { PokemonsContext } from '../contexts/pokemons.context';
import styles from '../styles/pagination.module.scss';

const Pagination = () => {
  const { getPokemonsByPage, currentPage, pageNumbers } = useContext(PokemonsContext);

  return (
    <nav className={styles.app_pagination}>
      <ul>
        {pageNumbers.map((number) => (
          <li key={number} style={{ backgroundColor : `${currentPage === number ? '#77afb7' : ''}`}}>
            {
              currentPage === number 
                ? (<span style={{ cursor: 'none' }}>{number}</span>) 
                : (<span onClick={() => getPokemonsByPage(number)}>{number}</span>)
            }
            
          </li>
        ))}
      </ul>
      <div className={styles.prev_next}>
        {
          currentPage === 1 
            ? (<span style={{ cursor: 'none', opacity: '0.5' }}>Prev</span>)
            : (<span onClick={() => getPokemonsByPage(currentPage - 1)}>Prev</span>)
        }
        <span onClick={() => getPokemonsByPage(currentPage + 1)}>Next</span>
      </div>
    </nav>
  )
}

export default Pagination;
// 77afb7
import React, { useContext } from "react";
import { PokemonsContext } from "../../contexts/pokemons.context";
import styles from '../../styles/pokemon-view.module.scss';
import Image from "next/image";
import { FiArrowRight } from "react-icons/fi";
import { useRouter } from "next/router";

const PokemonView = () => {
  const { pokemon } = useContext(PokemonsContext);
  const router = useRouter();

  return (
    <div className={styles.view_wrapper}>
      <div className={styles.header}>
        <span>{pokemon.name}</span>
        <span>#{pokemon.number}</span>
      </div>
      
      <div className={styles.content}>
        <div className={styles.content_image}>
          {pokemon.image && (
            <Image src={pokemon.image} alt={pokemon.name} width='200px' height='200px' />
          )}
        </div>
        <div className={styles.content_info}>
            <div className={styles.sub_title}>
              <h3>{pokemon.name}</h3>
              <span>{pokemon.classification}</span>
            </div>
            <div className={styles.sub_content}>
              
              <div className={styles.sub_content_info}>
                <h5 className={styles.sub_content_title}>Weight</h5>
                <div className={styles.sub_content_details}>
                  <div className={styles.sub_content_list}>
                    <span>Min:</span>
                    <span>{pokemon.weight?.minimum}</span>
                  </div>
                  <div className={styles.sub_content_list}>
                    <span>Max:</span>
                    <span>{pokemon.weight?.maximum}</span>
                  </div>
                </div>
              </div>

              <div className={styles.sub_content_info}>
                <h5 className={styles.sub_content_title}>Height</h5>
                <div className={styles.sub_content_details}>
                  <div className={styles.sub_content_list}>
                    <span>Min:</span>
                    <span>{pokemon.height?.minimum}</span>
                  </div>
                  <div className={styles.sub_content_list}>
                    <span>Max:</span>
                    <span>{pokemon.height?.maximum}</span>
                  </div>
                </div>
              </div>

              <div className={styles.view_details} onClick={() => router.push(`/pokemons/${pokemon.id}`)}>
                <button>
                  <span>View Details</span>
                  <span><FiArrowRight /></span>
                </button>
              </div>

            </div>
        </div>
      </div>
    </div>
  )
}

export default PokemonView;

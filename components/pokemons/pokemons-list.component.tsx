import React, { useContext, useState } from "react";
import { PokemonsContext } from "../../contexts/pokemons.context";
import styles from '../../styles/pokemons-list.module.scss'
import Pagination from "../pagination.component";
import { useRouter } from "next/router";

const PokemonsList = () => {
  const router = useRouter();
  const { pokemons, getPokemon, firstLetter } = useContext(PokemonsContext);

  return (
    <div className={styles.pokelist_wrapper}>
      <div className={styles.pokemon_wrapper}>
        {pokemons && pokemons.map(({ id, number, name }) => (
          <div key={id}>
            
            <div className={styles.pokemon} onClick={() => getPokemon(id)}>
              <div className={styles.avatar}>{firstLetter(name)}</div>
              <div className={styles.name}>
                <span>{number}</span>
                <span>{name}</span>
              </div>
            </div>

            <div className={styles.pokemon_mobile} onClick={() => router.push(`/pokemons/${id}`)}>
              <div className={styles.avatar}>{firstLetter(name)}</div>
              <div className={styles.name}>
                <span>{number}</span>
                <span>{name}</span>
              </div>
            </div>
          </div>
        ))}
      </div>
      <Pagination />
    </div>
  )
}

export default PokemonsList;
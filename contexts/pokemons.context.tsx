import React, { createContext } from 'react';
import usePokemons from '../hooks/use-pokemons';


interface Measurement {
  minimum: string;
  maximum: string
}

export interface Pokemon {
  id: string,
  name: string,
  number: string,
  image: string,
  classification: string,
  weight: Measurement,
  height: Measurement,
}

interface PokemonContextInterface {
  loader: boolean
  pokemons: Pokemon[]
  pokemon: Pokemon
  getPokemon: (id: String) => {}
  getPokemonsByPage: (page: number) => []
  currentPage: number
  pageNumbers: number[]
  firstLetter: (text: string) => ''
}


export const PokemonsContext = createContext({} as PokemonContextInterface)

const PokemonsProvider = ({ children}: any) => {
  const { ...rest } = usePokemons();
  const value: any = { ...rest }
  return(
    <PokemonsContext.Provider value={value}>
      {children}
    </PokemonsContext.Provider>
  )
}

export default PokemonsProvider;
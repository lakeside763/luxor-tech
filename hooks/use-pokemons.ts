import { useEffect, useState, useCallback } from 'react';
import { gql, useLazyQuery, useQuery } from '@apollo/client';

export const POKEMON_FRAGMENT = gql`
  fragment pokemonFields on Pokemon {
    id
    number
    name
    image
    classification
    weight {
      minimum
      maximum
    }
    height {
      minimum
      maximum
    }
  }
`;

export const GET_POKEMONS_LIST = gql`
  query GetPokemonsList($first: Int!) {
    pokemons(first: $first) {
      ...pokemonFields
    }
  }
  ${POKEMON_FRAGMENT}
`;

const usePokemons = () => {
  const [pokemons, setPokemons] = useState([]);
  const [pokemon, setPokemon] = useState({});
  const [loader, setLoader] = useState(true);
  const [pageNumbers, setPageNumbers] = useState([1, 2, 3, 4, 5]);
  const [currentPage, setCurrentPage] = useState(1);
  const { data, loading } = useQuery(GET_POKEMONS_LIST, { variables: { first: 10 } });
  const [getPokemonsListRequest, { data: pokemonsDataByPage, loading: loading_2 }] = useLazyQuery(GET_POKEMONS_LIST)

  useEffect(() => {
    if (data) {
      const { pokemons } = data;
      setPokemons(pokemons);
      setPokemon(pokemons[0]);
      setLoader(loading)
    }
  }, [data, loading]);

  useEffect(() => {
    if (pokemonsDataByPage) {
      const { pokemons } = pokemonsDataByPage;
      const pokemonsData = pokemons.slice(pokemons.length - 10);
      setPokemons(pokemonsData);
      setLoader(loading_2)
    }
  }, [pokemonsDataByPage, loading_2]);

  const getPokemon = (id: string) => {
    const pokemon = pokemons.find((poke: { id: string; }) => poke.id === id);
    if (pokemon) setPokemon(pokemon);
  }

  const pagination = useCallback((page: number) => {
    const midNumber = pageNumbers[2];
    const lastNumber = pageNumbers[4];
    const firstNumber = pageNumbers[0];
    if (page > midNumber) {
      const updatedPageNumbers = [ ...pageNumbers.slice(2), lastNumber + 1, lastNumber + 2 ]
      setPageNumbers(updatedPageNumbers);
    } else if (page < midNumber && page > 2) {
      const updatedPageNumbers = [ firstNumber - 2, firstNumber - 1, ...pageNumbers.slice(0, 3)];
      setPageNumbers(updatedPageNumbers);
    }
  }, [pageNumbers])


  const getPokemonsByPage = (page: number) => {
    getPokemonsListRequest({ variables: { first: page * 10 } });
    pagination(page);
    setCurrentPage(page);
  }

  const firstLetter = (text: string) => text.charAt(0);


  return {
    pokemons,
    pokemon,
    loader,
    getPokemon,
    getPokemonsByPage,
    currentPage,
    pageNumbers,
    firstLetter
  }
}

export default usePokemons;
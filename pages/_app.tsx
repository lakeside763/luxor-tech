import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import PokemonsProvider from '../contexts/pokemons.context';



export const client = new ApolloClient({
  uri: 'https://graphql-pokemon2.vercel.app',
  cache: new InMemoryCache({}),
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <PokemonsProvider>
        <Component {...pageProps} />
      </PokemonsProvider>
    </ApolloProvider>
  )
}

export default MyApp

import type { NextPage } from 'next'
import Head from 'next/head'
import Login from '../components/login.component';
import cookie from 'cookie';

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Luxor Tech</title>
        <meta name="description" content="Luxor tech portal login" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Login />

    </div>
  )
}

export async function getServerSideProps(context: any) {
  const parsedCookies = cookie.parse(`${context.req.headers.cookie}`);
  const { token } = parsedCookies;
  if (token) {
    context.res.statusCode = 302;
    context.res.setHeader('Location', '/pokemons');
  } 
  
  return {
    props: {}
  }
}

export default Home

import React, { Fragment, useEffect, useState } from "react";
import styles from '../../styles/pokemon-details.module.scss';
import Link from "next/link";
import { FiChevronRight } from "react-icons/fi";
import { useRouter } from "next/router";
import { gql, useQuery } from '@apollo/client';
import { POKEMON_FRAGMENT } from "../../hooks/use-pokemons";
import {  Pokemon } from '../../contexts/pokemons.context';
import cookie from 'cookie';
import Image from "next/image";
import Nav from "../../components/common/nav.component";
import Loader from "../../components/common/loader.component";
import { authCheck } from "../../utils/utils";

const GET_POKEMON_BY_ID = gql`
  query GetPokemonById($id: String) {
    pokemon(id: $id) {
      ...pokemonFields
    }
  }
  ${POKEMON_FRAGMENT}
`;


const PokemonDetailsPage = () => {
  const { query: { id } } = useRouter();
  const { loading, data } = useQuery(GET_POKEMON_BY_ID, { variables: { id } });
  const [pokemon, setPokemon] = useState<Pokemon>();

  useEffect(() => {
    if (data) {
      const { pokemon } = data;
      setPokemon(pokemon);
    }
  }, [data]);

  return (
    <div className={styles.page_wrapper}>
      <div className={styles.content_wrapper}>
        <Nav />

        {
          loading 
            ? (<Loader />)
            : (
              <Fragment>
                <div className={styles.header_wrapper}>
                  <header>
                    <h3>
                      <span>Pokemon: </span>
                      <span>#{pokemon?.number}</span>
                    </h3>
                  </header>
                  <div className={styles.bread_crumb}>
                    <span className={styles.link}>
                      <Link href='/pokemons'>
                        <a>Pokemons</a>
                      </Link>
                    </span>
                    <span className={styles.icon}>
                      <FiChevronRight className={styles.fi_icon} />
                    </span>
                    <span>{pokemon?.name}</span>
                  </div>
                </div>

                <div className={styles.container}>
                  <div className={styles.content}>
                    <div className={styles.content_image}>
                      {pokemon?.image && (
                        <Image src={pokemon.image} alt={pokemon.name} width='300px' height='300px' />
                      )}
                    </div>
                    <div className={styles.content_info}>
                        <div className={styles.sub_title}>
                          <h3>{pokemon?.name}</h3>
                          <span>{pokemon?.classification}</span>
                        </div>
                        <div className={styles.sub_content}>
                          
                          <div className={styles.sub_content_info}>
                            <h5 className={styles.sub_content_title}>Weight</h5>
                            <div className={styles.sub_content_details}>
                              <div className={styles.sub_content_list}>
                                <span>Min:</span>
                                <span>{pokemon?.weight?.minimum}</span>
                              </div>
                              <div className={styles.sub_content_list}>
                                <span>Max:</span>
                                <span>{pokemon?.weight?.maximum}</span>
                              </div>
                            </div>
                          </div>

                          <div className={styles.sub_content_info}>
                            <h5 className={styles.sub_content_title}>Height</h5>
                            <div className={styles.sub_content_details}>
                              <div className={styles.sub_content_list}>
                                <span>Min:</span>
                                <span>{pokemon?.height?.minimum}</span>
                              </div>
                              <div className={styles.sub_content_list}>
                                <span>Max:</span>
                                <span>{pokemon?.height?.maximum}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </Fragment>
            )
        }
      </div>
    </div>
  )
}

export async function getServerSideProps(context: any) {
  authCheck(context);
  
  return {
    props: {}
  }
}

export default PokemonDetailsPage;
import React, { useContext } from 'react';
import PokemonsList from '../../components/pokemons/pokemons-list.component';
import PokemonView from '../../components/pokemons/pokemon-view.component';
import styles from '../../styles/pokemons-page.module.scss';
import cookie from 'cookie';
import Nav from '../../components/common/nav.component';
import Loader from '../../components/common/loader.component';
import { PokemonsContext } from '../../contexts/pokemons.context';
import { authCheck } from '../../utils/utils';


const PokemonPage = () => {
  const { loader } = useContext(PokemonsContext);
  return (
    <div className={styles.page_wrapper}>
      <div className={styles.container}>
        <Nav />

        {loader 
          ? (<Loader />)
          : (
            <div className={styles.content_wrapper}>
              <div className={styles.pokemons_list}>
                <PokemonsList />
              </div>
              <div className={styles.pokemon_view}>
                <PokemonView />
              </div>
            </div>
          )
        }
      </div>
    </div>
  )
}

export async function getServerSideProps(context: any) {
  authCheck(context);
  
  return {
    props: {}
  }
}

export default PokemonPage;
import  cookie from 'cookie';

export const authCheck = (context: any) => {
  const parsedCookies = cookie.parse(`${context.req.headers.cookie}`);
  const { token } = parsedCookies;
  if (!token) {
    context.res.statusCode = 302;
    context.res.setHeader('Location', '/');
  } 
}


